# ParkingLot 

## How to run the application
### With Git
1. Clone the repository - ```git clone https://gitlab.com/hykim1228/ParkingLot.git```
2. ```cd ParkingLot```
3. ```./parkinglot.sh <path to test cases>``` 
   * Example: ```./parkinglot.sh src/main/resources/parkinglot_data.txt```   

### Without Git
1. Download the corresponding compressed file:
   * Linux: Download ```tar.gz``` file from Gitlab download option
2. ```tar -xvf <downloaded tar file, ex: ParkingLot-main.tar.gz>```
3. ```cd <ParkingLot-main or name u downloaded with>```
4. ```./parkinglot.sh <path to test cases>```
   * Example: ```./parkinglot.sh src/main/resources/parkinglot_data.txt```

## Explanation for outputs
1. If an action is invalid, it will output "Input is not valid"
2. If an action is valid:
    * If action is performed correctly - Desired output from the prompt is printed
    * If action is not performed correctly - "Reject" is printed with the reason.
    
