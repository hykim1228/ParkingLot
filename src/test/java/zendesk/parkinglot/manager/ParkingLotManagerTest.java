package zendesk.parkinglot.manager;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import zendesk.parkinglot.lot.CarLot;
import zendesk.parkinglot.lot.MotorcycleLot;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class ParkingLotManagerTest {
    private static final PrintStream standardOut = System.out;
    private static final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @BeforeClass
    public static void setTestManager() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @AfterClass
    public static void restore() {
        System.setOut(standardOut);
    }

    @Test
    public void testValidateInput_valid() {
        ParkingLotManager test = new ParkingLotManager();
        String validInput1 = "Enter motorcycle SGX1234A 1613541902";
        String validInput2 = "Enter car SGX1234A 1613541902";
        String validInput3 = "Exit SGX1234A 1613541902";

        Assert.assertTrue(test.validateInput(validInput1));
        Assert.assertTrue(test.validateInput(validInput2));
        Assert.assertTrue(test.validateInput(validInput3));
    }

    @Test
    public void testValidateInput_invalid() {
        ParkingLotManager testManager = new ParkingLotManager();
        String invalidInput1 = "Enter SGX1234A 1613541902";
        String invalidInput2 = "Enter car SGX1234A 1613541902 asdf";
        String invalidInput3 = "Exit asd SGX1234A 1613541902";
        String invalidInput4 = "Notexistcommand asd SGX1234A 1613541902";
        String invalidInput5 = "";
        String invalidInput6 = null;

        Assert.assertTrue(!testManager.validateInput(invalidInput1));
        Assert.assertTrue(!testManager.validateInput(invalidInput2));
        Assert.assertTrue(!testManager.validateInput(invalidInput3));
        Assert.assertTrue(!testManager.validateInput(invalidInput4));
        Assert.assertTrue(!testManager.validateInput(invalidInput5));
        Assert.assertTrue(!testManager.validateInput(invalidInput6));
    }

    @Test
    public void testPerformAction_invalid() {
        ParkingLotManager testManager = new ParkingLotManager();
        testManager.setParkingLot("car", new CarLot(3));
        testManager.setParkingLot("motorcycle", new MotorcycleLot(4));
        System.setOut(new PrintStream(outputStreamCaptor));
        String invalidInput1 = "NotExist SGX1234A 1613541902";
        testManager.performAction(invalidInput1);
        Assert.assertEquals("Action NotExist not found", outputStreamCaptor.toString().trim());
        System.setOut(standardOut);
    }

    @Test
    public void testPerformAction_enter_valid() {
        ParkingLotManager testManager = new ParkingLotManager();
        testManager.setParkingLot("car", new CarLot(3));
        testManager.setParkingLot("motorcycle", new MotorcycleLot(4));

        String[] input1 = new String[]{"Enter", "car", "SGX1234A", "1613541902"};
        String[] input2 = new String[]{"Enter", "motorcycle", "SGX12344A", "1613541902"};
        String[] input3 = new String[]{"Enter", "car", "SGX1233A", "1613541902"};

        String output1 = testManager.enterAction(input1[1], input1[2], input1[3]);
        Assert.assertEquals("Accept CarLot1", output1);

        String output2 = testManager.enterAction(input2[1], input2[2], input2[3]);
        Assert.assertEquals("Accept MotorcycleLot1", output2);

        String output3 = testManager.enterAction(input3[1], input3[2], input3[3]);
        Assert.assertEquals("Accept CarLot2", output3);
    }

    @Test
    public void testPerformAction_enter_invalid() {
        ParkingLotManager testManager = new ParkingLotManager();
        testManager.setParkingLot("car", new CarLot(3));
        testManager.setParkingLot("motorcycle", new MotorcycleLot(4));

        String[] input1 = new String[]{"Enter", "car", "SGX1234A", "161354s1902"};
        String[] input2 = new String[]{"Enter", "motorcycle", "SGX1234A", "1613541902"};
        String[] input3 = new String[]{"Enter", "motorcycle", "SGX1234A", "1613541902"};

        String output1 = testManager.enterAction(input1[1], input1[2], input1[3]);
        Assert.assertEquals("Reject: (Invalid Timestamp: 161354s1902)", output1);

        testManager.enterAction(input2[1], input2[2], input2[3]);
        String output3 = testManager.enterAction(input3[1], input3[2], input3[3]);
        Assert.assertEquals("Reject: (Motorcycle SGX1234A already exists in MotorcycleLot1)", output3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPerformAction_enterCycle_invalid() {
        ParkingLotManager testManager = new ParkingLotManager();
        testManager.setParkingLot("car", new CarLot(3));
        testManager.setParkingLot("motorcycle", new MotorcycleLot(4));

        String[] input1 = new String[]{"Enter", "cycle", "SGX1234A", "161354s1902"};
        testManager.enterAction(input1[1], input1[2], input1[3]);
    }

    @Test
    public void testPerformAction_exit_valid() {
        ParkingLotManager testManager = new ParkingLotManager();
        testManager.setParkingLot("car", new CarLot(3));
        testManager.setParkingLot("motorcycle", new MotorcycleLot(4));

        String[] input1 = new String[]{"Enter", "car", "SGX1234A", "1613541902"};
        testManager.enterAction(input1[1], input1[2], input1[3]);

        String[] input2 = new String[]{"Exit", "SGX1234A", "1613549102"};
        String output = testManager.exitAction(input2[1], input2[2]);
        Assert.assertEquals("CarLot1 4", output);
    }

    @Test
    public void testPerformAction_exit_invalid() {
        ParkingLotManager testManager = new ParkingLotManager();
        testManager.setParkingLot("car", new CarLot(3));
        testManager.setParkingLot("motorcycle", new MotorcycleLot(4));

        String[] input1 = new String[]{"Enter", "car", "SGX1234A", "1613541902"};
        testManager.enterAction(input1[1], input1[2], input1[3]);

        String[] input2 = new String[]{"Exit", "SGX1234A", "1613549s102"};
        String output2 = testManager.exitAction(input2[1], input2[2]);
        Assert.assertEquals("Reject: (Invalid Timestamp: 1613549s102)", output2);

        String[] input3 = new String[]{"Exit", "SGX1234AA", "1613549s102"};
        String output3 = testManager.exitAction(input3[1], input3[2]);
        Assert.assertEquals("Reject: (License SGX1234AA not found in any lots)", output3);
    }
}
