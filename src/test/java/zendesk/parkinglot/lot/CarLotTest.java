package zendesk.parkinglot.lot;

import org.junit.Test;
import zendesk.parkinglot.util.Constants;
import zendesk.parkinglot.vehicle.Car;
import zendesk.parkinglot.vehicle.Motorcycle;
import zendesk.parkinglot.vehicle.Vehicle;

public class CarLotTest {
    @Test
    public void testAddVehicle_valid() {
        CarLot lot = new CarLot(3);
        Vehicle v1 = new Car("aa");
        Vehicle v2 = new Car("bb");
        Vehicle v3 = new Car("cc");

        String lot1 = lot.addVehicle(v1, 1613541902);
        String lot2 = lot.addVehicle(v2, 1613541902);
        String lot3 = lot.addVehicle(v3, 1613541902);

        assert lot1.equals("CarLot1");
        assert lot2.equals("CarLot2");
        assert lot3.equals("CarLot3");
    }

    @Test(expected = IllegalStateException.class)
    public void testAddVehicle_lotFull_invalid() {
        CarLot lot = new CarLot(3);
        Vehicle v1 = new Car("aa");
        Vehicle v2 = new Car("bb");
        Vehicle v3 = new Car("cc");
        Vehicle invalid = new Car("dd");

        lot.addVehicle(v1, 1613541902);
        lot.addVehicle(v2, 1613541902);
        lot.addVehicle(v3, 1613541902);
        lot.addVehicle(invalid, 1613541902);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddVehicle_duplicateCar_invalid() {
        CarLot lot = new CarLot(3);
        Vehicle v1 = new Car("aa");
        Vehicle v2 = new Car("bb");
        Vehicle v3 = new Car("aa");

        lot.addVehicle(v1, 1613541902);
        lot.addVehicle(v2, 1613541902);
        lot.addVehicle(v3, 1613541902);
    }

    @Test(expected = ClassCastException.class)
    public void testAddVehicle_invalidVehicle_invalid() {
        CarLot lot = new CarLot(3);
        Vehicle v1 = new Car("aa");
        Vehicle v2 = new Car("bb");
        Vehicle v3 = new Motorcycle("cc");

        lot.addVehicle(v1, 1613541902);
        lot.addVehicle(v2, 1613541902);
        lot.addVehicle(v3, 1613541902);
    }

    @Test
    public void testRemoveVehicle_valid() {
        CarLot lot = new CarLot(3);
        Vehicle v1 = new Car("aa");
        Vehicle v2 = new Car("bb");
        Vehicle v3 = new Car("cc");

        lot.addVehicle(v1, 1613541902);
        lot.addVehicle(v2, 1613541902);
        lot.addVehicle(v3, 1613541902);

        int price = lot.removeVehicle("aa", 1613541902);

        assert price == 0;
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRemoveVehicle_invalid() {
        CarLot lot = new CarLot(3);
        Vehicle v1 = new Car("aa");
        Vehicle v2 = new Car("bb");
        Vehicle v3 = new Car("cc");

        lot.addVehicle(v1, 1613541902);
        lot.addVehicle(v2, 1613541902);
        lot.addVehicle(v3, 1613541902);

        lot.removeVehicle("aa", 1613541902);
        lot.removeVehicle("dd", 1613541902);
    }

    @Test
    public void testPrice_valid() {
        CarLot lot = new CarLot(3);
        Vehicle v1 = new Car("aa");
        Vehicle v2 = new Car("bb");
        Vehicle v3 = new Car("cc");

        lot.addVehicle(v1, 1613541902);
        lot.addVehicle(v2, 1613541902);
        lot.addVehicle(v3, 1613541902);

        // two hours
        int price1 = lot.removeVehicle("aa", 1613549102);
        // two hours and 2 mins
        int price2 = lot.removeVehicle("bb", 1613549222);

        assert price1 == Constants.CARLOT_PRICE * 2;
        assert price2 == Constants.CARLOT_PRICE * 3;
    }
}
