package zendesk.parkinglot.util;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DateHelperTest {
    class Helper {
        private long start;
        private long end;
        private int expected;

        public Helper(long start, long end, int expected) {
            this.start = start;
            this.end = end;
            this.expected = expected;
        }
    }

    @Test
    public void testGetHoursDiff() {
        List<Helper> testcases = new ArrayList<>(
            Arrays.asList(
                new Helper(1613541902, 1613549102, 2),
                new Helper(1613541902, 1613549222, 3),
                new Helper(1613541902, 1613559780, 5),
                new Helper(1613541902, 1613559960, 6)
            )
        );

        for(Helper test : testcases) {
            int got = DateHelper.getHoursDiff(test.start, test.end);
            assert got == test.expected;
        }
    }
}
