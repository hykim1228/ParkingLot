package zendesk.parkinglot.vehicle;

import org.junit.Assert;
import org.junit.Test;

public class VehicleManagerTest {
    @Test
    public void testCreateCar() {
        Vehicle car = VehicleManager.createVehicle("car", "test");
        Assert.assertTrue(car instanceof Car);
        Assert.assertTrue(car.getLicensePlate().equals("test"));
    }

    @Test
    public void testCreateMotorcycle() {
        Vehicle motorcycle = VehicleManager.createVehicle("motorcycle", "test");
        Assert.assertTrue(motorcycle instanceof Motorcycle);
        Assert.assertTrue(motorcycle.getLicensePlate().equals("test"));
    }

    @Test
    public void testCreateRandom() {
        Vehicle bike = VehicleManager.createVehicle("bike", "test");
        Assert.assertNull(bike);
    }
}
