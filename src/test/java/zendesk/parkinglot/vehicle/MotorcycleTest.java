package zendesk.parkinglot.vehicle;

import org.junit.Assert;
import org.junit.Test;

public class MotorcycleTest {
    @Test
    public void testCreateMotorcycle() {
        Motorcycle test = new Motorcycle("test");
        assert test.getLicensePlate() == "test";
    }

    @Test
    public void testSetLicense() {
        Motorcycle test = new Motorcycle("test");
        test.setLicensePlate("newtest");
        assert test.getLicensePlate() == "newtest";
    }

    @Test
    public void testEqual() {
        Motorcycle a = new Motorcycle("test");
        Motorcycle b = new Motorcycle("test");
        Assert.assertTrue(a.equals(b));
    }

    @Test
    public void testHashCodeEqual() {
        Motorcycle a = new Motorcycle("test");
        Motorcycle b = new Motorcycle("test");
        Assert.assertTrue(a.hashCode() == b.hashCode());
    }
}
