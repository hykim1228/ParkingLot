package zendesk.parkinglot.vehicle;

import org.junit.Assert;
import org.junit.Test;

public class CarTest {
    @Test
    public void testCreateCar() {
        Car test = new Car("test");
        assert test.getLicensePlate() == "test";
    }

    @Test
    public void testSetLicense() {
        Car test = new Car("test");
        test.setLicensePlate("newtest");
        assert test.getLicensePlate() == "newtest";
    }

    @Test
    public void testEqual() {
        Car a = new Car("test");
        Car b = new Car("test");
        Assert.assertTrue(a.equals(b));
    }

    @Test
    public void testHashCodeEqual() {
        Car a = new Car("test");
        Car b = new Car("test");
        Assert.assertTrue(a.hashCode() == b.hashCode());
    }
}
