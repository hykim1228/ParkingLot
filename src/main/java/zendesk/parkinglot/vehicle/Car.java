package zendesk.parkinglot.vehicle;

import java.util.Objects;

/**
 * @author Huiyeon Kim
 */
public class Car implements Vehicle {
    private String licensePlate;

    public Car(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    @Override
    public String getLicensePlate() {
        return this.licensePlate;
    }

    @Override
    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Objects.equals(this.licensePlate, car.licensePlate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.licensePlate);
    }
}
