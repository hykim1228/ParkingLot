package zendesk.parkinglot.vehicle;

import java.util.Objects;

/**
 * @author Huiyeon Kim
 */
public class Motorcycle implements Vehicle {
    private String licensePlate;

    public Motorcycle(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    @Override
    public String getLicensePlate() {
        return this.licensePlate;
    }

    @Override
    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Motorcycle motor = (Motorcycle) o;
        return Objects.equals(this.licensePlate, motor.licensePlate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.licensePlate);
    }
}
