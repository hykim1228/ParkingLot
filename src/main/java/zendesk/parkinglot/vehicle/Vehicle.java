package zendesk.parkinglot.vehicle;

/**
 * @author Huiyeon Kim
 */
public interface Vehicle {
    String getLicensePlate();
    void setLicensePlate(String licensePlate);
}
