package zendesk.parkinglot.vehicle;

import zendesk.parkinglot.util.Constants;

/**
 * @author Huiyeon Kim
 */
public class VehicleManager {
    /**
     * Method to create the corresponding Vehicle
     * @param type Vehicle Type
     * @param license license plate
     * @return corresponding vehicle or null if vehicle type does not exist
     */
    public static Vehicle createVehicle(String type, String license) {
        Vehicle v = null;
        switch (type) {
            case Constants.CAR_KEY:
                v = new Car(license);
                break;
            case Constants.MOTORCYCLE_KEY:
                v = new Motorcycle(license);
                break;
        }
        return v;
    }
}
