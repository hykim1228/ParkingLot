package zendesk.parkinglot.lot;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

import zendesk.parkinglot.util.DateHelper;
import zendesk.parkinglot.vehicle.Car;
import zendesk.parkinglot.vehicle.Vehicle;
import zendesk.parkinglot.util.Constants;

/**
 * @author Huiyeon Kim
 */
public class CarLot implements ParkingLot {
    private String lotName;
    private int price;
    private int capacity;
    private Map<Car, Long> cars; // Map of Car to its entry time
    private Map<Car, Integer> locations; // Map of Car to its location
    private TreeSet<Integer> freeSlots;

    public CarLot(int capacity) {
        this.lotName = Constants.CARLOT_NAME;
        this.price = Constants.CARLOT_PRICE;
        this.capacity = capacity;
        this.cars = new HashMap<>();
        this.locations = new HashMap<>();
        this.freeSlots = new TreeSet<>();
        for(int i=0;i<this.capacity;i++){
            freeSlots.add(i + 1);
        }
    }

    /**
     * Adds a vehicle to the CarLot and returns its location
     * @param v The vehicle to add
     * @param timestamp The time of entry
     * @return Location of vehicle
     * @throws IllegalArgumentException when car already exists in CarLot
     * @throws IllegalStateException when CarLot is full
     * @throws ClassCastException when Vehicle is not a car
     */
    @Override
    public String addVehicle(Vehicle v, long timestamp) throws IllegalArgumentException, IllegalStateException, ClassCastException {
        if(this.vehicleExists(v.getLicensePlate())) {
            throw new IllegalArgumentException("Car " + v.getLicensePlate() + " already exists in " + this.getVehicleLocation(v.getLicensePlate()));
        }
        if(this.freeSlots.size() == 0) {
            throw new IllegalStateException(this.lotName + " is full");
        }
        if(!(v instanceof Car)) {
            throw new ClassCastException(v.getLicensePlate() + " is not a Car");
        }
        Car c = (Car)v;
        int locationId = freeSlots.pollFirst();
        this.cars.put(c, timestamp);
        this.locations.put(c, locationId);
        return this.getVehicleLocation(v.getLicensePlate());
    }

    /**
     * Removes a vehicle from the Lot and returns its cost
     * @param license License plate of vehicle to remove
     * @param exitTime The time of exit
     * @return Cost to pay
     * @throws IllegalArgumentException when vehicle does not exist
     */
    @Override
    public int removeVehicle(String license, long exitTime) throws IllegalArgumentException {
        if(!this.vehicleExists(license)) {
            throw new IllegalArgumentException("Car " + license + " does not exist in CarLot");
        }

        Car car = new Car(license);
        long entryTime = this.cars.remove(car);
        int freedSlot = this.locations.remove(car);
        this.freeSlots.add(freedSlot);

        return this.calculateCost(entryTime, exitTime);
    }

    /**
     * Returns whether a vehicle exists in the Lot
     * @param license License of Vehicle
     * @return Boolean
     */
    @Override
    public boolean vehicleExists(String license) {
        Car tempCar = new Car(license);
        return this.cars.containsKey(tempCar);
    }

    /**
     * Retrieves the location of the vehicle
     * @param license License plate of Vehicle
     * @return String location of the vehicle
     * @throws IllegalArgumentException when vehicle does not exists in the Lot
     */
    @Override
    public String getVehicleLocation(String license) throws IllegalArgumentException {
        Car tempCar = new Car(license);
        if(!this.cars.containsKey(tempCar)) {
            throw new IllegalArgumentException("license: " + license +" not found");
        }
        return this.lotName + this.locations.get(tempCar);
    }

    @Override
    public String getLotName() {
        return this.lotName;
    }

    @Override
    public int getCapacity() {
        return this.capacity;
    }

    @Override
    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    /**
     * Retrieves cost of parking
     * @param entryTime entry time of vehicle
     * @param exitTime exit time of vehicle
     * @return price
     */
    private int calculateCost(long entryTime, long exitTime) {
        return DateHelper.getHoursDiff(entryTime, exitTime) * this.price;
    }
}
