package zendesk.parkinglot.lot;

import zendesk.parkinglot.vehicle.Vehicle;

/**
 * @author Huiyeon Kim
 */
public interface ParkingLot {
    String addVehicle(Vehicle v, long timestamp);
    int removeVehicle(String license, long timestamp);
    boolean vehicleExists(String license);
    String getVehicleLocation(String license);
    String getLotName();
    int getCapacity();
    void setCapacity(int capacity);
}
