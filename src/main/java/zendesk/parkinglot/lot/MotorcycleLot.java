package zendesk.parkinglot.lot;

import zendesk.parkinglot.util.Constants;
import zendesk.parkinglot.util.DateHelper;
import zendesk.parkinglot.vehicle.Car;
import zendesk.parkinglot.vehicle.Motorcycle;
import zendesk.parkinglot.vehicle.Vehicle;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

/**
 * @author Huiyeon Kim
 */
public class MotorcycleLot implements ParkingLot {
    private String lotName;
    private int capacity;
    private int price;
    private Map<Motorcycle, Long> motorcycles;
    private Map<Motorcycle, Integer> locations;
    private TreeSet<Integer> freeSlots;

    public MotorcycleLot(int capacity) {
        this.lotName = Constants.MOTORCYCLELOT_NAME;
        this.price = Constants.MOTORCYCLELOT_PRICE;
        this.capacity = capacity;
        this.motorcycles = new HashMap<>();
        this.locations = new HashMap<>();
        this.freeSlots = new TreeSet<>();
        for (int i = 0; i < this.capacity; i++) {
            freeSlots.add(i + 1);
        }
    }

    /**
     * Adds a vehicle to the MotorcycleLot and returns its location
     * @param v The vehicle to add
     * @param timestamp The time of entry
     * @return Location of vehicle
     * @throws IllegalArgumentException when vehicle already exists in MotorcycleLot
     * @throws IllegalStateException when MotorcycleLot is full
     * @throws ClassCastException when Vehicle is not a car
     */
    @Override
    public String addVehicle(Vehicle v, long timestamp) throws IllegalArgumentException, IllegalStateException, ClassCastException {
        if (this.vehicleExists(v.getLicensePlate())) {
            throw new IllegalArgumentException("Motorcycle " + v.getLicensePlate() + " already exists in " + this.getVehicleLocation(v.getLicensePlate()));
        }
        if (this.freeSlots.size() == 0) {
            throw new IllegalStateException(this.lotName + " is full");
        }
        if(!(v instanceof Motorcycle)) {
            throw new ClassCastException(v.getLicensePlate() + " is not a Motorcycle");
        }

        Motorcycle c = (Motorcycle) v;
        int locationId = freeSlots.pollFirst();
        this.motorcycles.put(c, timestamp);
        this.locations.put(c, locationId);
        return this.lotName + locationId;
    }

    /**
     * Removes a vehicle from the Lot and returns its cost
     * @param license License plate of vehicle to remove
     * @param exitTime The time of exit
     * @return Cost to pay
     * @throws IllegalArgumentException when vehicle does not exist
     */
    @Override
    public int removeVehicle(String license, long exitTime) throws IllegalArgumentException {
        if (!this.vehicleExists(license)) {
            throw new IllegalArgumentException("motorcycle " + license + " does not exist in MotorcycleLot");
        }

        Motorcycle motorcycle = new Motorcycle(license);
        long entryTime = this.motorcycles.remove(motorcycle);
        int freedSlot = this.locations.remove(motorcycle);
        this.freeSlots.add(freedSlot);

        return this.calculateCost(entryTime, exitTime);
    }

    /**
     * Returns whether a vehicle exists in the Lot
     * @param license License of Vehicle
     * @return Boolean
     */
    @Override
    public boolean vehicleExists(String license) {
        Motorcycle tempMotorcycle = new Motorcycle(license);
        return this.motorcycles.containsKey(tempMotorcycle);
    }

    /**
     * Retrieves the location of the vehicle
     * @param license License plate of Vehicle
     * @return String location of the vehicle
     * @throws IllegalArgumentException when vehicle does not exists in the Lot
     */
    @Override
    public String getVehicleLocation(String license) throws IllegalArgumentException {
        Motorcycle tempMotorcycle = new Motorcycle(license);
        if (!this.motorcycles.containsKey(tempMotorcycle)) {
            throw new IllegalArgumentException("license: " + license + " not found");
        }
        return this.lotName + this.locations.get(tempMotorcycle);
    }

    @Override
    public String getLotName() {
        return this.lotName;
    }

    @Override
    public int getCapacity() {
        return this.capacity;
    }

    @Override
    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    /**
     * Retrieves cost of parking
     * @param entryTime entry time of vehicle
     * @param exitTime exit time of vehicle
     * @return price
     */
    private int calculateCost(long entryTime, long exitTime) {
        return DateHelper.getHoursDiff(entryTime, exitTime) * this.price;
    }
}

