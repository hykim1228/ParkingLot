package zendesk.parkinglot.manager;

import zendesk.parkinglot.lot.ParkingLot;
import zendesk.parkinglot.util.Constants;
import zendesk.parkinglot.vehicle.Vehicle;
import zendesk.parkinglot.vehicle.VehicleManager;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Huiyeon Kim
 */
public class ParkingLotManager {
    private Map<String, ParkingLot> parkingLots;
    private Map<String, Integer> commandValidationMap;

    public ParkingLotManager() {
        this.parkingLots = new HashMap<>();
        this.commandValidationMap = new HashMap<>();
        this.commandValidationMap.put(Constants.ACTION_ENTER, Constants.ACTION_ENTER_INPUT_SIZE);
        this.commandValidationMap.put(Constants.ACTION_EXIT, Constants.ACTION_EXIT_INPUT_SIZE);
    }

    /**
     * Takes a line in the input file and validates whether the command has the correct amount of arguments
     * @param input line of input
     * @return boolean whether number of arguments are correct
     */
    public boolean validateInput(String input) {
        if(input == null || input.equals("") || input.length() == 0) {
            return false;
        }
        String[] inputs = input.split(" ");
        String action = inputs[0];
        switch (action) {
            case Constants.ACTION_ENTER:
                return inputs.length == this.commandValidationMap.get(Constants.ACTION_ENTER);

            case Constants.ACTION_EXIT:
                return inputs.length == this.commandValidationMap.get(Constants.ACTION_EXIT);
        }

        return false;
    }

    /**
     * Adds a new parking lot to the ParkingLot Manager
     * @param lotName Identifier of the lot
     * @param lot the ParkingLot Object
     */
    public void setParkingLot(String lotName, ParkingLot lot) {
        this.parkingLots.put(lotName, lot);
    }

    /**
     * Takes a line in the input file and performs the corresponding action and prints the result
     * @param input line of input
     */
    public void performAction(String input) {
        String[] inputs = input.split(" ");
        String action = inputs[0];
        switch (action) {
            case Constants.ACTION_ENTER:
                String output = this.enterAction(inputs[1], inputs[2], inputs[3]);
                System.out.println(output);
                break;

            case Constants.ACTION_EXIT:
                String exitOutput = this.exitAction(inputs[1], inputs[2]);
                System.out.println(exitOutput);
                break;

            default:
                System.out.println("Action " + action + " not found");
        }
    }

    /**
     * Function to perform "Enter" action and prints result
     * @param type Vehicle type
     * @param license license plate number for Vehicle
     * @param timestampStr Entry time in string
     * @return String which represents the result
     */
    public String enterAction(String type, String license, String timestampStr) {
        if (!this.parkingLots.containsKey(type)) {
            throw new IllegalArgumentException("ParkingLot for " + type + " does not exist");
        }
        ParkingLot lot = this.parkingLots.get(type);
        Vehicle vehicle = VehicleManager.createVehicle(type, license);
        try {
            long timestamp = Long.parseLong(timestampStr);
            String lotName = lot.addVehicle(vehicle, timestamp);
            return "Accept " + lotName;
        } catch (NumberFormatException e) {
            return "Reject: (Invalid Timestamp: " + timestampStr + ")";
        } catch (Exception e) {
            return "Reject: (" + e.getMessage() + ")";
        }
    }


    /**
     * Function to perform "Exit" action and prints result
     * @param license license plate number for Vehicle
     * @param timestampStr Exit time in string
     * @return String which represents the result
     */
    public String exitAction(String license, String timestampStr) {
        for(ParkingLot lot : this.parkingLots.values()) {
            if(lot.vehicleExists(license)) {
                try {
                    long timestamp = Long.parseLong(timestampStr);
                    String location = lot.getVehicleLocation(license);
                    int price = lot.removeVehicle(license, timestamp);
                    return location + " " + price;
                } catch (NumberFormatException e) {
                   return "Reject: (Invalid Timestamp: " + timestampStr + ")";
                } catch(Exception e) {
                    return "Reject: (" + e.getMessage() + ")";
                }
            }
        }

        return "Reject: (License " + license + " not found in any lots)";
    }
}
