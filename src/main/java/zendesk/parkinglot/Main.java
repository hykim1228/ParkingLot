package zendesk.parkinglot;

import zendesk.parkinglot.manager.ParkingLotManager;
import zendesk.parkinglot.lot.CarLot;
import zendesk.parkinglot.lot.MotorcycleLot;
import zendesk.parkinglot.util.Constants;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * @author Huiyeon Kim
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("\n");
        System.out.println("*******************************************************************");
        System.out.println("*******************************************************************");
        System.out.println("******************      Zendesk PARKING LOT     *******************");
        System.out.println("*******************************************************************");
        System.out.println("*******************************************************************");
        System.out.println();
        if(args.length == 0) {
            System.out.println("Please Enter the file path to the parking lot data");
            return;
        }

        ParkingLotManager manager = new ParkingLotManager();
        try {
            File actionsFile = new File(args[0]);
            if(actionsFile.length() == 0) {
                System.out.println("File: " + args[1] + " is empty");
                return;
            }
            Scanner actionsReader = new Scanner(actionsFile);
            String init = actionsReader.nextLine();
            String[] initSize = init.split(" ");

            int carLotSize = Integer.parseInt(initSize[0]);
            int motorcycleLotSize = Integer.parseInt(initSize[1]);
            manager.setParkingLot(Constants.CAR_KEY, new CarLot(carLotSize));
            manager.setParkingLot(Constants.MOTORCYCLE_KEY, new MotorcycleLot(motorcycleLotSize));

            while (actionsReader.hasNextLine()) {
                String input = actionsReader.nextLine();
                if (!manager.validateInput(input)) {
                    System.out.println("Input is not valid");
                    continue;
                }
                manager.performAction(input);
            }
            actionsReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("File: " + args[1] + " is not found.");
        } catch (NumberFormatException e) {
            System.out.println("Error creating parking lots, please put a valid number of parking lot capacity");
        }
    }
}
