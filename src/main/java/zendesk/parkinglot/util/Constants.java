package zendesk.parkinglot.util;

/**
 * @author Huiyeon Kim
 */
public class Constants {
    // Lot Information
    public static final String CARLOT_NAME = "CarLot";
    public static final int CARLOT_PRICE = 2;
    public static final String MOTORCYCLELOT_NAME = "MotorcycleLot";
    public static final int MOTORCYCLELOT_PRICE = 1;

    // Lot Keys
    public static final String CAR_KEY = "car";
    public static final String MOTORCYCLE_KEY = "motorcycle";

    // Actions
    public static final String ACTION_ENTER = "Enter";
    public static final int ACTION_ENTER_INPUT_SIZE = 4;
    public static final String ACTION_EXIT = "Exit";
    public static final int ACTION_EXIT_INPUT_SIZE = 3;
}
