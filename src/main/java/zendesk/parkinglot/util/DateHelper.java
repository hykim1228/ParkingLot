package zendesk.parkinglot.util;

/**
 * @author Huiyeon Kim
 */
public class DateHelper {
    /**
     * Gets the difference of hours of two unix timestamps
     * @param start start time
     * @param end end time
     * @return diff in hours
     */
    public static int getHoursDiff(long start, long end) {
        long diff = end - start;
        int diffHours = (int) (diff / (60 * 60));
        if(diff % (60 * 60) != 0) {
            diffHours++;
        }
        return diffHours;
    }
}
