#!/bin/bash
arg1=$1
##directory where jar file is located
dir=target
##jar file name
jar_name=ParkingLot-1.0-SNAPSHOT.jar

if ! [ -x "$(command -v java)" ]
then
    sudo apt-get update
    sudo apt-get install -y openjdk-8-jdk
fi

if ! [ -x "$(command -v mvn)" ]
then
    sudo apt-get update
    sudo apt-get -y install maven
fi

mvn clean install
java -jar $dir/$jar_name $arg1
